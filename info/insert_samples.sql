insert into Grupo
  (nome, valor)
values
  ("Carnes e derivados", 70),
  ("Frutas e derivados", 67),
  ("Leguminosas e derivados", 84),
  ("Cereais e derivados", 94);

insert into Componente
  (nome, unidade, vdr, dt_reg)
values
  ("Valor Energético", "kcal", 2000, "2024-05-02"),
  ("Carboidratos", "g", 300, "2024-05-02"),
  ("Açucares adicionados", "g", 50, "2024-05-02"),
  ("Proteínas", "g", 50, "2024-05-02"),
  ("Gorduras totais", "g", 65, "2024-05-02"),
  ("Colesterol", "mg", 300, "2024-05-02"),
  ("Fibras Alimentares", "g", 25, "2024-05-02"),
  ("Sódio", "mg", 2000, "2024-05-02");

insert into Receita
(
  nome,
  descricao,
  tamanho_porcao, un_porcao, medida_cas, porcoes_cas,
  dt_criacao, dt_modif
)
values
(
  "Bolo de abacaxi",
  "Bolo de abacaxi caseiro com canela e erva doce",
  75, "g", "Pedaço", 1,
  "1999-03-23", "2003-03-21"
),
(
  "Caldo de Mandioca",
  "Caldo de mandioca apimentado com carne bovina e ervas aromáticas",
  200, "ml", "Colher de sopa", 10,
  "2003-05-23", "2013-10-07"
),
(
  "Arroz carreteiro",
  "Delicioso arroz carreteiro, usando os melhores miúdos para dar o sabor",
  150, "g", "Colher de sopa", 10,
  "1984-02-19", "2016-01-14"
);

insert into Ingrediente
(
  codigo, nome,
  dt_reg, id_grupo
)
values
(
  "BK82852", "Cenoura, s/ casca, crua",
  "2024-05-02", 3
),
(
  "BK82753", "Abacaxi, s/ casca, crua",
  "2021-05-01", 2
),
(
  "BK85751", "Arroz, cozido",
  "2009-02-10", 4
),
(
  "BK82332", "Trigo, farinha, fortificada",
  "2008-10-20", 4
),
(
  "BK93859", "Carne bovina, acem, cozida",
  "2023-07-06", 1
),
(
  "BK82752", "Mandioca, s/ casca, cozida",
  "1997-01-18", 3
);
   
insert into IngredRec
  (id_ingred, id_receit, quantidade)
values
  (2, 1, 2500),
  (4, 1, 5000),
  (1, 2, 2500),
  (5, 2, 3500),
  (6, 2, 8000),
  (1, 3, 1000),
  (3, 3, 7500),
  (5, 3, 2500),
  (6, 3, 1000);

insert into IngredComp
  (id_ingred, id_comp, dt_reg, quantidade)
values
	(1, 1, "2024-05-04", 7203),
 	(1, 2, "2024-05-04", 4632),
 	(1, 3, "2024-05-04", 8265),
 	(1, 4, "2024-05-04", 7339),
 	(1, 5, "2024-05-04", 1335),
 	(1, 6, "2024-05-04", 1250),
 	(1, 7, "2024-05-04", 9516),
 	(1, 8, "2024-05-04", 9704),
 	(2, 1, "2024-05-04", 1942),
 	(2, 2, "2024-05-04", 7395),
 	(2, 3, "2024-05-04", 2085),
 	(2, 4, "2024-05-04", 7726),
 	(2, 5, "2024-05-04", 2973),
 	(2, 6, "2024-05-04", 4399),
 	(2, 7, "2024-05-04", 1997),
 	(2, 8, "2024-05-04", 6532),
 	(3, 1, "2024-05-04", 8338),
 	(3, 2, "2024-05-04", 3403),
 	(3, 3, "2024-05-04", 2497),
 	(3, 4, "2024-05-04", 3885),
 	(3, 5, "2024-05-04", 1693),
 	(3, 6, "2024-05-04", 7105),
 	(3, 7, "2024-05-04", 5348),
 	(3, 8, "2024-05-04", 1602),
 	(4, 1, "2024-05-04", 1460),
 	(4, 2, "2024-05-04", 1839),
 	(4, 3, "2024-05-04", 5360),
 	(4, 4, "2024-05-04", 9955),
 	(4, 5, "2024-05-04", 311),
 	(4, 6, "2024-05-04", 780),
 	(4, 7, "2024-05-04", 190),
 	(4, 8, "2024-05-04", 3384),
 	(5, 1, "2024-05-04", 5003),
 	(5, 2, "2024-05-04", 4756),
 	(5, 3, "2024-05-04", 1431),
 	(5, 4, "2024-05-04", 6316),
 	(5, 5, "2024-05-04", 7236),
 	(5, 6, "2024-05-04", 1516),
 	(5, 7, "2024-05-04", 579),
 	(5, 8, "2024-05-04", 9115),
 	(6, 1, "2024-05-04", 1332),
 	(6, 2, "2024-05-04", 6512),
 	(6, 3, "2024-05-04", 4472),
 	(6, 4, "2024-05-04", 944),
 	(6, 5, "2024-05-04", 9119),
 	(6, 6, "2024-05-04", 8449),
 	(6, 7, "2024-05-04", 4049),
 	(6, 8, "2024-05-04", 2959);
