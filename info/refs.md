 > BRASIL. AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA (ANVISA). . Rotulagem nutricional : novas regras entram em vigor em 120 dias. 2022. Disponível em: https://www.gov.br/anvisa/pt-br/assuntos/noticias-anvisa/2022/rotulagem-nutricional-novas-regras-entram-em-vigor-em-120-dias. Acesso em: 26 jul. 2023.

 > BRASIL. INSTRUÇÃO NORMATIVA – IN N° 75, DE 8 DE OUTUBRO DE 2020. Brasília, Disponível em: http://antigo.anvisa.gov.br/documents/10181/3882585/%283%29IN_75_2020_COMP.pdf/e5a331f2-86db-4bc8-9f39-afb6c1d7e19f. Acesso em: 26 jul. 2023.

 > BRASIL. AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA (ANVISA). Principais mudanças e modelos. 2022. Disponível em: https://www.gov.br/anvisa/pt-br/assuntos/alimentos/rotulagem/principais-mudancas-e-modelos. Acesso em: 26 jul. 2023._

 > BRASIL. AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA (ANVISA). Perguntas & Respostas: rotulagem nutricional de alimentos embalados. Brasilia: Ministério da Saúde, 2023. Disponível em: https://www.gov.br/anvisa/pt-br/centraisdeconteudo/publicacoes/alimentos/perguntas-e-respostas-arquivos/rotulagem-nutricional_2a-edicao.pdf. Acesso em: 26 jul. 2023

 > BRASIL. RESOLUÇÃO-RDC Nº 259, DE 20 DE SETEMBRO DE 2002. Brasília, Disponível em: https://bvsms.saude.gov.br/bvs/saudelegis/anvisa/2002/rdc0259_20_09_2002.html. Acesso em: 26 jul. 2023.

 > BRASIL. AGêNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA (ANVISA). . Perguntas & Respostas: requisitos para o uso de gordura trans industriais em alimentos. Brasilia: Ministério da Saúde, 2021. Disponível em: https://www.gov.br/anvisa/pt-br/centraisdeconteudo/publicacoes/alimentos/perguntas-e-respostas-arquivos/gorduras-trans-industriais.pdf. Acesso em: 26 jul. 2023.

 > BRASIL. IMPRENSA NACIONAL. Instrução Normativa IN, nº 75 de 08 de Outubro de 2020. Brasília: Ministério da Casa Civil, 2020. Disponível em: https://www.in.gov.br/web/dou/-/instrucao-normativa-in-n-75-de-8-de-outubro-de-2020-282071143. Acesso em: 20 mai. 2024.
