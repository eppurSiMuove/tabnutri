LEVANTAMENTO DE REQUISITOS 
 
REQUISITOS FUNCIONAIS

RF0- O sistema deve seguir a instrução normativa nº75 de 2020 da Anvisa.
RF1- O sistema deve se basear nas informações do site TBCA (Tabela Brasileira de Composição de Alimentos).
RF2- O sistema deve possibilitar impressão das tabelas nutricionais em forma de tabela ou texto puro.
RF3- O sistema deve calcular as informações nutricionais baseadas em receitas previamente cadastrada.
RF4- O sistema deve possibilitar o cadastramento de receitas. 
RF5- O sistema deve conter interface intuitiva.

REQUISITOS NÃO FUNCIONAIS

RNF0- O sistema deve conter tecnologia open source.
RNF1- O sistema deve conter as receitas cadastradas salvas off-line. 
