/* show all foods of a specific group */
select
  i.codigo as Código,
  i.nome as Ingrediente,
  i.dt_reg as Registro
from 
  Ingrediente i
inner join
  Grupo g on g.id = i.id_grupo
where
  g.nome = 'Cereais e derivados';

/* list all ingredients of a recipe */
select
  i.nome as Ingrediente,
  ir.quantidade as Quantidade
from
  Ingrediente i
inner join
  IngredRec ir on ir.id_ingred = i.id
inner join
  Receita r on r.id = ir.id_receit
where
  r.nome = 'Caldo de Mandioca';

/* list components and its amount for a specific ingredient */
select
  c.nome as Componente,
  ic.quantidade as Quantidade
from
  Componente c
inner join
  IngredComp ic on ic.id_comp = c.id
inner join
  Ingrediente i on ic.id_ingred = i.id
where
  i.nome = 'Arroz, cozido';

/* ingredients amount summation of all recipes */
select
  r.nome as Receita,
  sum(ir.quantidade) as TotalIngredientes
from
  IngredRec ir
inner join
  Receita r on r.id = ir.id_receit
group by
  r.nome;
  
/* ingredients amount summation of a specific recipe */
select
  sum(ir.quantidade) as TotalIngredientes
from
  IngredRec ir
inner join
  Receita r on r.id = ir.id_receit
where
  r.nome = 'Caldo de Mandioca';

/* ingredient amount for each 100g of the recipe */
/* IngredAmountBy100g = ((100 * IngredAmountInRecipe) / TotalRecipeAmount) */
select
  ir.id_ingred as IngredID,
  ((100 * ir.quantidade) / (
    select
      sum(ir.quantidade)
    from
      IngredRec ir
    where
      ir.id_receit = 2)
  ) as IngQttBy100Rec
from
  IngredRec ir
where
  ir.id_receit = 2;

/*Component amount by ingredient by 100g of recipe*/
select
  ic.id_comp,
  ic.quantidade, (
  ic.quantidade * (select
    ((100 * ir.quantidade) / (select
      sum(ir.quantidade)
    from
      IngredRec ir
    where
      ir.id_receit = 1)
    )
  from
    IngredRec ir
  where
    ir.id_receit = 1 and ir.id_ingred = 10) / 100) as compByRecByIng
from
  IngredComp ic
inner join
  IngredRec ir on ir.id_ingred = ic.id_ingred
where
  ir.id_receit = 1 and ir.id_ingred = 10;
