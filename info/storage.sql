create table Grupo (
  id integer primary key not null,
  nome text not null unique,
  valor integer not null unique
);

create table Ingrediente (
  id integer primary key not null,
  codigo text not null unique,
  nome text not null,
  dt_reg text not null,
  id_grupo integer default 0 not null,
  foreign key (id_grupo) references Grupo(id)
    on delete set default
    on update cascade
);

create table Componente(
  id integer primary key not null,
  nome text not null,
  unidade text not null,
  vdr real not null,
  dt_reg text not null
);

create table IngredComp (
  id_ingred integer not null,
  id_comp integer default 0 not null,
  quantidade real not null,
  unidade text not null,
  dt_reg text not null,
  foreign key (id_ingred) references Ingrediente(id)
    on delete cascade
    on update cascade,
  foreign key (id_comp) references Componente(id)
    on delete set default
    on update cascade
);

create table Receita (
  id integer primary key not null,
  nome text not null,
  descricao text,
  tamanho_porcao real not null,
  un_porcao text not null,
  medida_cas text not null,
  porcoes_cas real not null,
  dt_criacao text not null,
  dt_modif text not null
);

create table IngredRec (
  id_ingred integer not null,
  id_receit integer not null,
  quantidade real not null,
  foreign key (id_ingred) references Ingrediente(id)
    on delete restrict
    on update restrict,
  foreign key (id_receit) references Receita(id)
    on delete cascade
    on update cascade
);
