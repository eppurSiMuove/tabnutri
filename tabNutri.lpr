program tabNutri;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  lazcontrols,
  SysUtils,

  { you can add units after this }
  uMain,
  uDMMain,
  uRecipe,
  uIngredient,
  uComponent,
  uGroup,
  uTBCAPageHandler, uIN75PageHandler, uPageHandler, uNFLabel;

{$R *.res}

begin
  DefaultFormatSettings.DecimalSeparator:= '.';
  DefaultFormatSettings.DateSeparator:= '-';
  DefaultFormatSettings.ShortDateFormat:= 'yyyy-mm-dd';
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

