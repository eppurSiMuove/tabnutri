unit uComponent;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLdb,

  uDMMain;

type

  { TNFComponent }

  TNFComponent = class
    private
      FID: integer;
      FName: string;
      FCreation: TDateTime;
      FUnit: string;
      FRDV: single;
      FQtty: single;
      FQt100Rec: single;
      FPerc100Rec: single;
      FQtPortionRec: single;
      FPercPortionRec: single;
      function getID: integer;
      function getName: string;
      function getPerc100Rec: single;
      function getPercPortionRec: single;
      function getQt100Rec: single;
      function getQtPortionRec: single;
      function getQtty: single;
      function getCreation: TDateTime;
      function getUnit: string;
      function getRDV: single;
      procedure setID(AValue: integer);
      procedure setName(AValue: string);
      procedure setPerc100Rec(AValue: single);
      procedure setPercPortionRec(AValue: single);
      procedure setQt100Rec(AValue: single);
      procedure setQtPortionRec(AValue: single);
      procedure setQtty(AValue: single);
      procedure setCreation(AValue: TDateTime);
      procedure setUnit(AValue: string);
      procedure setRDV(AValue: single);
    public
      constructor Create; overload;
      constructor Create(AComponentID: integer; AQuantity, AQtty100Rec, AQttyPortionRec: single); overload;

      property id: integer read getID write setID;
      property name: string read getName write setName;
      property creation: TDateTime read getCreation write setCreation;
      property unitMeasurement: string read getUnit write setUnit;
      property rdv: single read getRDV write setRDV;
      property quantity: single read getQtty write setQtty;
      property qtty100Rec: single read getQt100Rec write setQt100Rec;
      property percent100Rec: single read getPerc100Rec write setPerc100Rec;
      property qttyPortionRec: single read getQtPortionRec write setQtPortionRec;
      property percentPortionRec: single read getPercPortionRec write setPercPortionRec;

      function save: integer;
      function delete: integer;
  end;

  TNFComponents = array of TNFComponent;

implementation

{ TNFComponent }

function TNFComponent.getID: integer;
const
  sql = 'select id from Componente where nome = :name;';
begin
  if (FID = -1) then begin
     with dmMain do begin
       prepareNewSQL(qryAux, sql);
       qryAux.ParamByName('name').AsString:= Self.name;
       qryAux.Open;
       FID:= qryAux.FieldByName('id').AsInteger;
     end;
  end;
  result:= FID;
end;

function TNFComponent.getName: string;
begin
  result:= FName;
end;

function TNFComponent.getPerc100Rec: single;
begin
  result:= FPerc100Rec;
end;

function TNFComponent.getPercPortionRec: single;
begin
  result:= FPercPortionRec;
end;

function TNFComponent.getQt100Rec: single;
begin
  result:= FQt100Rec;
end;

function TNFComponent.getQtPortionRec: single;
begin
  result:= FQtPortionRec;
end;

function TNFComponent.getQtty: single;
begin
  result:= FQtty;
end;

function TNFComponent.getCreation: TDateTime;
begin
  result:= FCreation;
end;

function TNFComponent.getUnit: string;
begin
  result:= FUnit;
end;

function TNFComponent.getRDV: single;
begin
  result:= FRDV;
end;

procedure TNFComponent.setID(AValue: integer);
begin
  if AValue = FID then Exit;
  FID:= AValue;
end;

procedure TNFComponent.setName(AValue: string);
begin
  if AValue = FName then Exit;
  FName:= AValue;
end;

procedure TNFComponent.setPerc100Rec(AValue: single);
begin
  if AValue = FPerc100Rec then Exit;
  FPerc100Rec:= AValue;
end;

procedure TNFComponent.setPercPortionRec(AValue: single);
begin
  if AValue = FPercPortionRec then Exit;
  FPercPortionRec:= AValue;
end;

procedure TNFComponent.setQt100Rec(AValue: single);
begin
  if AValue = FQt100Rec then Exit;
  FQt100Rec:= AValue;
end;

procedure TNFComponent.setQtPortionRec(AValue: single);
begin
  if AValue = FQtPortionRec then Exit;
  FQtPortionRec:= AValue;
end;

procedure TNFComponent.setQtty(AValue: single);
begin
  if AValue = FQtty then Exit;
  FQtty:= AValue;
end;

procedure TNFComponent.setCreation(AValue: TDateTime);
begin
  if AValue = FCreation then Exit;
  FCreation:= AValue;
end;

procedure TNFComponent.setUnit(AValue: string);
begin
  if AValue = FUnit then Exit;
  FUnit:= AValue;
end;

procedure TNFComponent.setRDV(AValue: single);
begin
  if AValue = FRDV then Exit;
  FRDV:= AValue;
end;

constructor TNFComponent.Create;
begin
  inherited Create;
  Self.id:= -1;
end;

constructor TNFComponent.Create(AComponentID: integer; AQuantity, AQtty100Rec, AQttyPortionRec: single);
var
  auxQuery: TSQLQuery;
  sql: string;
begin
  sql:= 'select * from Componente where id = :id;';

  with dmMain do begin
    newQuery(auxQuery);
    prepareNewSQL(auxQuery, sql);
    auxQuery.ParamByName('id').AsInteger:= AComponentID;
    auxQuery.Open;

    Self.id:= auxQuery.FieldByName('id').AsInteger;
    Self.name:= auxQuery.FieldByName('nome').AsString;
    Self.creation:= StrToDate(auxQuery.FieldByName('dt_reg').AsString, 'yyyy-mm-dd');
    Self.unitMeasurement:= auxQuery.FieldByName('unidade').AsString;
    Self.rdv:= auxQuery.FieldByName('vdr').AsFloat;
    Self.quantity:= AQuantity;
    Self.qtty100Rec:= AQtty100Rec;
    Self.qttyPortionRec:= AQttyPortionRec;
  end;

  FreeAndNil(auxQuery);
end;

function TNFComponent.save: integer;
begin

end;

function TNFComponent.delete: integer;
begin

end;

end.

