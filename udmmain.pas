unit uDMMain;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Controls, DB, SQLite3Conn, SQLDB, SQLite3Dyn;

type

  { TdmMain }

  TdmMain = class(TDataModule)
    imgList: TImageList;
    connection: TSQLite3Connection;
    scriptCreateDB: TSQLScript;
    query: TSQLQuery;
    qryAux: TSQLQuery;
    transaction: TSQLTransaction;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure defineLibLocation;
    procedure createDataBase;
  public
    procedure prepareNewSQL(var AQuery: TSQLQuery; ASQL: string);
    procedure newQuery(var AQuery: TSQLQuery);
  end;

var
  dmMain: TdmMain;

implementation

{$R *.lfm}

{ TdmMain }

procedure TdmMain.DataModuleCreate(Sender: TObject);
begin
  defineLibLocation;
  createDataBase;
end;

procedure TdmMain.defineLibLocation;
begin
  {$IFDEF UNIX}  // Linux
    {$IFNDEF DARWIN}
      SQLiteDefaultLibrary := '/usr/lib/libsqlite3.so';
    {$ENDIF}
    {$IFDEF DARWIN}
      SQLiteLibraryName:='/usr/lib/libsqlite3.dylib';
    {$ENDIF}
  {$ENDIF}

  {$IFDEF WINDOWS} // Windows
    SQLiteDefaultLibrary := 'sqlite3.dll';
  {$ENDIF}
end;

procedure TdmMain.createDataBase;
const
  dbname = 'tabNutri.s3db';
begin
  connection.DatabaseName:= dbname;
  connection.Close; // Ensure the connection is closed when we start

  // check whether the file already exists
  if not FileExists(dbname) then begin

    try  // Create the database and the tables
      connection.Open;
      transaction.Active:= true;
      scriptCreateDB.ExecuteScript;
      transaction.Commit;
    except
      on e: Exception do
        raise e;
    end;

  end;
end;

procedure TdmMain.prepareNewSQL(var AQuery: TSQLQuery; ASQL: string);
begin
  AQuery.Active:= false;
  AQuery.Fields.Clear;
  AQuery.SQL.Clear;
  AQuery.SQL.Text:= ASQL;
end;

procedure TdmMain.newQuery(var AQuery: TSQLQuery);
begin
  //if not (AQuery = nil) then FreeAndNil(AQuery);
  AQuery:= TSQLQuery.Create(nil);
  AQuery.DataBase:= connection;
  AQuery.Transaction:= transaction;
end;

end.

