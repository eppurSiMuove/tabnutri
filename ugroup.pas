unit uGroup;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, uIngredient;

type

  { TGroup }

  TGroup = class
    protected
      FID: integer;
      FName: string;
      FValue: integer;
      FIngreds: TIngredients;
    private
      function getID: integer;
      function getIngreds: TIngredients;
      function getName: string;
      function getValue: integer;
      procedure setID(AValue: integer);
      procedure setIngreds(AValue: TIngredients);
      procedure setName(AValue: string);
      procedure setValue(AValue: integer);

    public
      property id: integer read getID write setID;
      property name: string read getName write setName;
      property value: integer read getValue write setValue;
      property ingredients: TIngredients read getIngreds write setIngreds;
  end;

implementation

{ TGroup }

function TGroup.getID: integer;
begin
  result:= FID;
end;

function TGroup.getIngreds: TIngredients;
begin
  result:= FIngreds;
end;

function TGroup.getName: string;
begin
  result:= FName;
end;

function TGroup.getValue: integer;
begin
  result:= FValue;
end;

procedure TGroup.setID(AValue: integer);
begin
  if AValue = FID then Exit;
  FID:= AValue;
end;

procedure TGroup.setIngreds(AValue: TIngredients);
begin
  if AValue = FIngreds then Exit;
  FIngreds:= AValue;
end;

procedure TGroup.setName(AValue: string);
begin
  if AValue = FName then Exit;
  FName:= AValue;
end;

procedure TGroup.setValue(AValue: integer);
begin
  if AValue = FValue then Exit;
  FValue:= AValue;
end;

end.

