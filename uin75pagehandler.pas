unit uIN75PageHandler;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, OpenSSLSockets, SAX_HTML, DOM_HTML, DOM,

  uPageHandler, uDMMain;

type

  { TIN75PageHandler }

  TIN75PageHandler = class(TPageHandler)
    private
      function getRawPage: string;
      function hasTBCAName(var AIN75Component: string): Boolean;
    public
      constructor Create;
      function getDailyValues: TStringList;
      function updateDailyValuesData: Boolean;
      function dailyValuesDataIsOld: Boolean;
  end;

implementation

{ TIN75PageHandler }

function TIN75PageHandler.getRawPage: string;
var
  page: string;
begin
  try
    page:= Self.FHttpClient.Get(Self.mainURL);
    result:= page;
  except
    on E: Exception do
      raise E;
  end;
end;

constructor TIN75PageHandler.Create;
begin
  FMainURL:= 'https://www.in.gov.br/web/dou/-/instrucao-normativa-in-n-75-de-8-de-outubro-de-2020-282071143';
  inherited Create;
end;

function TIN75PageHandler.getDailyValues: TStringList;
var
  rawPage, component, vuField, value, unitMeas: string;
  doc      : THTMLDocument;
  dvs      : TStringList;
  tbody    : TDOMNode;
  trows    : TDOMNodeList;
  tmpFile  : Textfile;
  r, rows  : integer;
const
  tmpHTML = 'tmp.html';
begin
  try
    rawPage:= getRawPage;
    rawPage:= StringReplace(rawPage, #13, '', [rfReplaceAll]);
    rawPage:= StringReplace(rawPage, #10, '', [rfReplaceAll]);

    AssignFile(tmpFile, tmpHTML);
    Rewrite(tmpFile);
    WriteLn(tmpFile, rawPage);
    CloseFile(tmpFile);

    ReadHTMLFile(doc, tmpHTML);

    tbody  := doc.GetElementsByTagName('tbody')[1];
    trows  := tbody.ChildNodes;
    rows   := trows.Count;
    r      := 1;

    dvs    := TStringList.Create;
    while (r < rows - 1) do begin

      r:= r + 1;
                     //<tr>    /<td><p>      /TextContent
      component:= Trim(trows[r].ChildNodes[0].TextContent);
      if not hasTBCAName(component) then Continue;

      vufield  := Trim(trows[r].ChildNodes[1].TextContent);
      value    := vuField.Split(' ')[0];
      value    := StringReplace(value, '.', '', []);
      value    := StringReplace(value, ',', '.', []);
      unitMeas := vuField.Split(' ')[1];
      dvs.Add(component);
      dvs.Add(value);
      dvs.Add(unitMeas);
    end;
    result:= dvs
  except
    on E: Exception do
      raise E;
  end;
end;

function TIN75PageHandler.updateDailyValuesData: Boolean;
var
  sql: string;
  data: TStringList;
  c: integer;
begin
  sql:= 'delete from Componente;';
  with dmMain do begin
    prepareNewSQL(query, sql);
    query.ExecSQL;
  end;

  data:= TStringList.Create;
  data:= getDailyValues;

  sql:=       'insert into Componente ';
  sql:= sql + '(nome, unidade, vdr, dt_reg) ';
  sql:= sql + 'values ';
  sql:= sql + '(:name, :unit, :rdv, :creation);';

  c:=0;
  while (c < data.Count) do begin
    try
      with dmMain do begin
        prepareNewSQL(query, sql);
        query.ParamByName('name').AsString     := data[c];
        query.ParamByName('rdv').AsFloat       := StrToFloat(data[c + 1]);
        query.ParamByName('unit').AsString     := data[c + 2];
        query.ParamByName('creation').AsString := FormatDateTime('yyyy-mm-dd', Now);
        query.ExecSQL;
        c:= c + 3;
      end;
    except
      on E: Exception do begin
        dmMain.transaction.Rollback;
        result:= false;
        raise E;
      end;
    end;
  end;
  dmMain.transaction.Commit;
  result:= true;
end;

function TIN75PageHandler.dailyValuesDataIsOld: Boolean;
var
  output: integer;
  sql: string;
begin
  sql:= 'SELECT (SELECT date("now", "-1 month")) > (SELECT dt_reg from Componente WHERE id = 1) as isOld;';
  with dmMain do begin
    prepareNewSQL(query, sql);
    query.Open;
    if query.FieldByName('isOld').isNull then Exit(true);

    output:= query.FieldByName('isOld').AsInteger;
    if output = 1 then Exit(true) else Exit(false);
  end;
end;

function TIN75PageHandler.hasTBCAName(var AIN75Component: string): Boolean;
var
  in75Names: array of string = (
    'Valor energético', 'Carboidratos', 'Açúcares adicionados', 'Proteínas',
    'Gorduras totais', 'Gorduras saturadas', 'Gorduras trans', 'Gorduras monoinsaturadas',
    'Gorduras poli-insaturadas', 'Ômega 6', 'Ômega 3', 'Colesterol', 'Fibras alimentares',
    'Sódio', 'Vitamina A', 'Vitamina D', 'Vitamina E', 'Vitamina K', 'Vitamina C', 'Tiamina',
    'Riboflavina', 'Niacina', 'Vitamina B6', 'Biotina', 'Ácido fólico', 'Ácido pantotênico',
    'Vitamina B12', 'Cálcio', 'Cloreto', 'Cobre', 'Cromo', 'Ferro', 'Flúor', 'Fósforo', 'Iodo',
    'Magnésio', 'Manganês', 'Molibdênio', 'Potássio', 'Selênio', 'Zinco', 'Colina'
  );
  tbcaNames: array of string = (
    'Energia', 'Carboidrato total', 'Açúcar de adição', 'Proteína',
    'Lipídios', 'Ácidos graxos saturados', 'Ácidos graxos trans', 'Ácidos graxos monoinsaturados',
    'Ácidos graxos poliinsaturados', '', '', 'Colesterol', 'Fibra alimentar',
    'Sódio', 'Vitamina A (RE)', 'Vitamina D', 'Alfa-tocoferol (Vitamina E)', '', 'Vitamina C', 'Tiamina',
    'Riboflavina', 'Niacina', 'Vitamina B6', '', 'Equivalente de folato', '',
    'Vitamina B12', 'Cálcio', '', 'Cobre', '', 'Ferro', '', 'Fósforo', '',
    'Magnésio', 'Manganês', '', 'Potássio', 'Selênio', 'Zinco', ''
  );
  idx: integer;
begin
  for idx:=0 to High(in75Names) do begin
    if AIN75Component = in75Names[idx] then begin
      if not tbcaNames[idx].IsEmpty then Exit(true) else Exit(false);
    end;
  end;
  Exit(false);
end;

end.

