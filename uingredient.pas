unit uIngredient;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLdb,

  uDMMain, uComponent;

type

  { TIngredient }

  TIngredient = class
    private
      FID: integer;
      FGID: integer;
      FCode: string;
      FName: string;
      FGroup: string;
      FCreation: TDateTime;
      FQtty: single;
      FQttBy100: single;
      FQttByPor: single;
      FNFComponents: TNFComponents;
      function getCod: string;
      function getGroup: string;
      function getID: integer;
      function getIngred: string;
      function getNFComponents: TNFComponents;
      function getQttBy100Rec: single;
      function getQttByPortion: single;
      function getQtty: single;
      function getCreation: TDateTime;
      procedure setCod(AValue: string);
      procedure setGroup(AValue: string);
      procedure setID(AValue: integer);
      procedure setIngred(AValue: string);
      procedure setNFComponents(AValue: TNFComponents);
      procedure setQttBy100Rec(AValue: single);
      procedure setQttByPortion(AValue: single);
      procedure setQtty(AValue: single);
      procedure setCreation(AValue: TDateTime);
    public
      constructor Create; overload;
      constructor Create(ARecipeID, AIngredientID: integer;
                         AQuantity, APortionRec: single); overload;
      destructor Destroy; override;
      property id: integer read getID write setID;
      property code: string read getCod write setCod;
      property name: string read getIngred write setIngred;
      property group: string read getGroup write setGroup;
      property creation: TDateTime read getCreation write setCreation;
      property quantity: single read getQtty write setQtty;
      property qttBy100Rec: single read getQttBy100Rec write setQttBy100Rec;
      property qttByPortion: single read getQttByPortion write setQttByPortion;
      property components: TNFComponents read getNFComponents write setNFComponents;
      function save: Boolean;
      function getComponentMass(AComponentID: integer): single;
  end;

  TIngredients = array of TIngredient;

implementation

{ TIngredient }

function TIngredient.getCod: string;
begin
  result:= FCode;
end;

function TIngredient.getGroup: string;
begin
  result:= FGroup;
end;

function TIngredient.getID: integer;
var
  sql: string;
  pk: integer;
begin
  if (FID = 0) and not (Self.code.IsEmpty) then begin
    with dmMain do begin
      try
        sql:= 'select id from Ingrediente where codigo = :code;';
        prepareNewSQL(qryAux, sql);
        qryAux.ParamByName('code').AsString:= Self.code;
        qryAux.Open;

        pk:= qryAux.FieldByName('id').AsInteger;

        if pk > 0 then begin
          Self.id:= pk;
          Exit(pk);
        end;
      except
        on E: Exception do begin
          Self.id:= 0;
          raise E;
          Exit(0);
        end;
      end;
    end;
  end else begin
    result:= FID;
  end;
end;

function TIngredient.getIngred: string;
begin
  result:= FName;
end;

function TIngredient.getNFComponents: TNFComponents;
begin
  result:= FNFComponents;
end;

function TIngredient.getQttBy100Rec: single;
begin
  result:= FQttBy100;
end;

function TIngredient.getQttByPortion: single;
begin
  result:= FQttByPor;
end;

function TIngredient.getQtty: single;
begin
  result:=FQtty;
end;

function TIngredient.getCreation: TDateTime;
begin
  result:= FCreation;
end;

procedure TIngredient.setCod(AValue: string);
begin
  if AValue = FCode then Exit;
  FCode:= AValue;
end;

procedure TIngredient.setGroup(AValue: string);
var
  sql: string;
begin
  if AValue = FGroup then Exit;

  FGroup:= AValue;
  sql:= 'select id from Grupo where nome = upper(:name);';
  try
    with dmMain do begin
      prepareNewSQL(qryAux, sql);
      qryAux.ParamByName('name').AsString:= Self.group;
      qryAux.Open;
      Self.FGID:= qryAux.FieldByName('id').AsInteger;
    end;
  except
    on E: Exception do
      raise E;
  end;
end;

procedure TIngredient.setID(AValue: integer);
begin
  if AValue = FID then Exit;
  FID:= AValue;
end;

procedure TIngredient.setIngred(AValue: string);
begin
  if AValue = FName then Exit;
  FName:= AValue;
end;

procedure TIngredient.setNFComponents(AValue: TNFComponents);
begin
  if AValue = FNFComponents then Exit;
  FNFComponents:= AValue;
end;

procedure TIngredient.setQttBy100Rec(AValue: single);
begin
  if AValue = FQttBy100 then Exit;
  FQttBy100:= AValue;
end;

procedure TIngredient.setQttByPortion(AValue: single);
begin
  if AValue = FQttByPor then Exit;
  FQttByPor:= AValue;
end;

procedure TIngredient.setQtty(AValue: single);
begin
  if AValue = FQtty then Exit;
  FQtty:= AValue;
end;

procedure TIngredient.setCreation(AValue: TDateTime);
begin
  if AValue = FCreation then Exit;
  FCreation:= AValue;
end;

constructor TIngredient.Create;
begin
  inherited Create;
  Self.id:= 0;
end;

constructor TIngredient.Create(ARecipeID, AIngredientID: integer; AQuantity, APortionRec: single);
var
  sql: string;
  auxQuery: TSQLQuery;
  component: TNFComponent;
  id_grp, id_comp, c: integer;
  qt_comp, compByIngByRec100, compByIngByRecPortion: single;
begin
  inherited Create;

  sql:= 'select * from Ingrediente where id = :id;';
  dmMain.newQuery(auxQuery);
  dmMain.prepareNewSQL(auxQuery, sql);
  auxQuery.ParamByName('id').AsInteger:= AIngredientID;
  auxQuery.Open;

  Self.id:= auxQuery.FieldByName('id').AsInteger;
  Self.code:= auxQuery.FieldByName('codigo').AsString;
  Self.name:= auxQuery.FieldByName('nome').AsString;
  Self.creation:= StrToDate(auxQuery.FieldByName('dt_reg').AsString, 'yyyy-mm-dd');
  Self.quantity:= AQuantity;
  id_grp:= auxQuery.FieldByName('id_grupo').AsInteger;

  sql:= 'select nome from Grupo where id = :id;';
  dmMain.prepareNewSQL(auxQuery, sql);
  auxQuery.ParamByName('id').AsInteger:= id_grp;
  auxQuery.Open;
  Self.group:= auxQuery.FieldByName('nome').AsString;

  sql:= 'select '+
        '  ir.id_ingred as IngredID, ' +
        '  ((:qtRec * ir.quantidade) / ( '+
        '    select '+
        '      sum(ir.quantidade) '+
        '    from '+
        '      IngredRec ir '+
        '    where '+
        '      ir.id_receit = :recID) '+
        '  ) as IngQttByQtRec '+
        'from '+
        '  IngredRec ir '+
        'where '+
        '  ir.id_receit = :recID and ir.id_ingred = :ingID;';

  dmMain.prepareNewSQL(auxQuery, sql);
  auxQuery.ParamByName('qtRec').AsInteger:= 100;
  auxQuery.ParamByName('recID').AsInteger:= ARecipeID;
  auxQuery.ParamByName('ingID').AsInteger:= AIngredientID;
  auxQuery.Open;
  Self.qttBy100Rec:= auxQuery.FieldByName('IngQttByQtRec').AsFloat;

  dmMain.prepareNewSQL(auxQuery, sql);
  auxQuery.ParamByName('qtRec').AsFloat:= APortionRec;
  auxQuery.ParamByName('recID').AsInteger:= ARecipeID;
  auxQuery.ParamByName('ingID').AsInteger:= AIngredientID;
  auxQuery.Open;
  Self.qttByPortion:= auxQuery.FieldByName('IngQttByQtRec').AsFloat;

  sql:= 'select' +
        '  ic.id_comp as compID, ' +
        '  ic.quantidade as compQtty, (' +
        '  ic.quantidade * (select ' +
        '    ((100 * ir.quantidade) / (select ' +
        '      sum(ir.quantidade) ' +
        '    from ' +
        '      IngredRec ir ' +
        '    where ' +
        '      ir.id_receit = :recID) ' +
        '    ) ' +
        '  from ' +
        '    IngredRec ir ' +
        '  where ' +
        '    ir.id_receit = :recID and ir.id_ingred = :ingID) / 100) as compByIngByRec100, (' +
        '  ic.quantidade * (select ' +
        '    ((:recPortion * ir.quantidade) / (select ' +
        '      sum(ir.quantidade) ' +
        '    from ' +
        '      IngredRec ir ' +
        '    where ' +
        '      ir.id_receit = :recID) ' +
        '    ) ' +
        '  from ' +
        '    IngredRec ir ' +
        '  where ' +
        '    ir.id_receit = :recID and ir.id_ingred = :ingID) / 100) as compByIngByRecPortion ' +
        'from ' +
        '  IngredComp ic ' +
        'inner join ' +
        '  IngredRec ir on ir.id_ingred = ic.id_ingred ' +
        'where ' +
        '  ir.id_receit = :recID and ir.id_ingred = :ingID;';

  dmMain.prepareNewSQL(auxQuery, sql);
  auxQuery.ParamByName('recID').AsInteger:= ARecipeID;
  auxQuery.ParamByName('ingID').AsInteger:= AIngredientID;
  auxQuery.ParamByName('recPortion').AsFloat:= APortionRec;
  auxQuery.Open;
  auxQuery.Last;
  SetLength(FNFComponents, auxQuery.RecordCount);
  auxQuery.First;
  c:= 0;

  repeat
    id_comp:= auxQuery.FieldByName('compID').AsInteger;
    qt_comp:= auxQuery.FieldByName('compQtty').AsFloat;
    compByIngByRec100:= auxQuery.FieldByName('compByIngByRec100').AsFloat;
    compByIngByRecPortion:= auxQuery.FieldByName('compByIngByRecPortion').AsFloat;
    // TODO: handle unit conversion
    component:= TNFComponent.Create(id_comp, qt_comp, compByIngByRec100, compByIngByRecPortion);
    component.percent100Rec:= (100 * component.qtty100Rec) / component.rdv;
    component.percentPortionRec:= (100 * component.qttyPortionRec) / component.rdv;
    Self.components[c]:= component;
    auxQuery.Next;
    c:= c + 1;
  until auxQuery.EOF;

  FreeAndNil(auxQuery);
end;

destructor TIngredient.Destroy;
begin

  inherited destroy;
end;

function TIngredient.save: Boolean;
var
  auxQuery: TSQLQuery;
  sql: string;
  c, nComps, nIngreds: integer;
begin
  try
    sql:=       'select ';
    sql:= sql + '  count(ic.id_ingred) as nComps, count(i.id) as nIngreds ';
    sql:= sql + 'from ';
    sql:= sql + '  IngredComp ic ';
    sql:= sql + 'right join ';
    sql:= sql + '  Ingrediente i on i.id = ic.id_ingred ';
    sql:= sql + 'where ';
    sql:= sql + '  i.codigo = :code ';
    sql:= sql + 'order by ';
    sql:= sql + '  i.id;';
    dmMain.newQuery(auxQuery);
    dmMain.prepareNewSQL(auxQuery, sql);
    auxQuery.ParamByName('code').AsString:= Self.code;
    auxQuery.Open;
    nComps:= auxQuery.FieldByName('nComps').AsInteger;
    nIngreds:= auxQuery.FieldByName('nIngreds').AsInteger;

    if nComps > 0 then begin
      Exit(true);
    end else begin
      if nIngreds = 0 then begin
        sql:=       'insert into Ingrediente (codigo, nome, dt_reg, id_grupo) ';
        sql:= sql + 'values (:code, :name, :creation, :gid);';
        dmMain.prepareNewSQL(auxQuery, sql);
        auxQuery.ParamByName('code').AsString:= Self.code;
        auxQuery.ParamByName('name').AsString:= Self.name;
        auxQuery.ParamByName('gid').AsInteger:= Self.FGID;
        auxQuery.ParamByName('creation').AsString:= FormatDateTime('yyyy-mm-dd', Self.creation);
        auxQuery.ExecSQL;
        dmMain.transaction.Commit;

        sql:= 'select last_insert_rowid() as last_id;';
        dmMain.prepareNewSQL(auxQuery, sql);
        auxQuery.Open;
        Self.id:= auxQuery.FieldByName('last_id').AsInteger;
      end;

      sql:=       'insert into IngredComp ';
      sql:= sql + '  ( id_ingred,  id_comp,  quantidade, unidade,   dt_reg) ';
      sql:= sql + 'values (:id_ingred, :id_comp, :quantidade, :unidade, :dt_reg);';
      dmMain.prepareNewSQL(auxQuery, sql);
      for c:=0 to High(Self.components) do begin
        auxQuery.ParamByName('id_ingred').AsInteger:= Self.id;
        auxQuery.ParamByName('id_comp').AsInteger:= Self.components[c].id;
        auxQuery.ParamByName('quantidade').AsFloat:= Self.components[c].quantity;
        auxQuery.ParamByName('unidade').AsString:= Self.components[c].unitMeasurement;
        auxQuery.ParamByName('dt_reg').AsString:= FormatDateTime('yyyy-mm-dd', Self.components[c].creation);
        auxQuery.ExecSQL;
      end;
      dmMain.transaction.Commit;
      FreeAndNil(auxQuery);
      Exit(true);
    end;
  except
    dmMain.transaction.Rollback;
    FreeAndNil(auxQuery);
    Exit(false);
  end;
end;

function TIngredient.getComponentMass(AComponentID: integer): single;
var
  component: TNFComponent;
begin
  for component in Self.components do begin
    if (component.id = AComponentID) then begin
      result:= (Self.quantity * component.quantity) / 100;
      break;
    end;
  end;
end;

end.

