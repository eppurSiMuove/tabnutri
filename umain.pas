unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  Buttons, ExtCtrls, Spin, EditBtn, ListViewFilterEdit, LCLIntf,

  uTBCAPageHandler, uIN75PageHandler, uIngredient, uRecipe, uNFLabel, uDMMain;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    btnBackToHome: TSpeedButton;
    btnBackToRecipes: TSpeedButton;
    btnCancelIngred: TSpeedButton;
    btnBackIngred: TSpeedButton;
    btnCopyNFText: TSpeedButton;
    btnClearIngred: TSpeedButton;
    btnDelCancelRecipe: TSpeedButton;
    btnPrintLabel: TSpeedButton;
    btnEditSaveRecipe: TSpeedButton;
    btnSaveIngred: TSpeedButton;
    btnChooseIngred: TSpeedButton;
    btnSearchAddIngred: TBitBtn;
    btnRegRecipe: TBitBtn;
    btnPrintTicket: TBitBtn;
    dteRecipeModif: TDateEdit;
    dteRecipeCreation: TDateEdit;
    edtQtdIngred: TFloatSpinEdit;
    edtHomeMeasureQtt: TFloatSpinEdit;
    edtPortionQtt: TFloatSpinEdit;
    edtSearchIngred: TEdit;
    edtRecipeName: TEdit;
    edtHomeMeasureName: TEdit;
    gbxActionRecipe: TGroupBox;
    gbxPortion: TGroupBox;
    gbxHomeMeasure: TGroupBox;
    gbxRegRecipe: TGroupBox;
    gbxAddIngred: TGroupBox;
    gbxActionIngred: TGroupBox;
    lbRecipeModif: TLabel;
    lbRecipeCreation: TLabel;
    lbRecipeName: TLabel;
    lbPortionQtt: TLabel;
    lbHomeMeasureQtt: TLabel;
    lbSubTitle: TLabel;
    lbTitle: TLabel;
    lvChooseIngredient: TListView;
    lvChooseRecipe: TListView;
    lvfChooseIngredient: TListViewFilterEdit;
    lvAddIngred: TListView;
    lvfChooseRecipe: TListViewFilterEdit;
    mmTextNFLabel: TMemo;
    mmRecipeDesc: TMemo;
    pnTbsTextNFLabel: TPanel;
    pnTbsRegIngred: TPanel;
    pnTbsSearchIngred: TPanel;
    pnTbsRegRecipe: TPanel;
    pnTbsHome: TPanel;
    pgcMain: TPageControl;
    btnDeleteIngredient: TSpeedButton;
    btnSaveRecipe: TSpeedButton;
    btnCancelRecipe: TSpeedButton;
    pnTbsChooseRecipe: TPanel;
    rbLabelText: TRadioButton;
    rbLabelTable: TRadioButton;
    rgLabelType: TRadioGroup;
    statusBar: TStatusBar;
    tbsTextNFLabel: TTabSheet;
    tbsGenerateLabel: TTabSheet;
    tbsSearchIngred: TTabSheet;
    tbsRegIngredient: TTabSheet;
    tbsHome: TTabSheet;
    tbsRegRecipe: TTabSheet;
    procedure btnBackIngredClick(Sender: TObject);
    procedure btnBackToHomeClick(Sender: TObject);
    procedure btnBackToRecipesClick(Sender: TObject);
    procedure btnCancelIngredClick(Sender: TObject);
    procedure btnCancelRecipeClick(Sender: TObject);
    procedure btnChooseIngredClick(Sender: TObject);
    procedure btnCopyNFTextClick(Sender: TObject);
    procedure btnDelCancelRecipeClick(Sender: TObject);
    procedure btnDeleteIngredientClick(Sender: TObject);
    procedure btnEditSaveRecipeClick(Sender: TObject);
    procedure btnPrintLabelClick(Sender: TObject);
    procedure btnPrintTicketClick(Sender: TObject);
    procedure btnRegRecipeClick(Sender: TObject);
    procedure btnSaveIngredClick(Sender: TObject);
    procedure btnSaveRecipeClick(Sender: TObject);
    procedure btnSearchAddIngredClick(Sender: TObject);
    procedure edtHomeMeasureQttKeyPress(Sender: TObject; var Key: char);
    procedure edtPortionQttKeyPress(Sender: TObject; var Key: char);
    procedure edtQtdIngredKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvAddIngredEdited(Sender: TObject; Item: TListItem; var AValue: string);
    procedure lvChooseIngredientDblClick(Sender: TObject);
  private
    CurrentRecipe: TRecipe;
    NFLabel: TNFLabel;
    procedure drawFoodList(foodList: TStringList);
    procedure ingredientToRecipeList;
    procedure fillRecipeList(ingredients: TIngredients);
    procedure updateGroupAndComponentData;
    procedure clearTextFields(fields: array of TWinControl);
    function checkRequiredFields(fields: array of TWinControl): Boolean;
  public

  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.btnRegRecipeClick(Sender: TObject);
begin
  tbsRegRecipe.Show;
end;

procedure TfrmMain.btnSaveIngredClick(Sender: TObject);
var
  inputs: array of TWinControl;
begin
  if (Length(CurrentRecipe.ingredients) < 1) then begin
    ShowMessage('A receita deve conter ao menos um ingrediente.');
    Exit;
  end;

  try
    if (pnTbsRegRecipe.Caption = 'Cadastrar receita') then begin
      if CurrentRecipe.save then begin
        if CurrentRecipe.saveIngredients then begin
          ShowMessage('Receita gravada com sucesso.');
        end else begin
          TRecipe.exclude(CurrentRecipe.id);
          ShowMessage('Ocorreu um erro ao gravar os ingredientes.');
        end;
      end else begin
        ShowMessage('Ocorreu um erro ao gravar a receita.');
      end;
    end else begin
      if CurrentRecipe.update then begin
        ShowMessage('Receita atualizada com sucesso.');
      end else begin
        ShowMessage('Ocorreu um erro ao atualizar a receita.');
      end;
    end;
  finally
    inputs:= [
      edtRecipeName,
      dteRecipeCreation,
      edtPortionQtt,
      edtHomeMeasureQtt,
      edtHomeMeasureName,
      mmRecipeDesc
    ];
    clearTextFields(inputs);
    edtSearchIngred.Clear;
    lvAddIngred.Items.Clear;
    pnTbsRegIngred.Caption:='Ingredientes';
    FreeAndNil(CurrentRecipe);
    tbsHome.Show;
  end;
end;

procedure TfrmMain.btnSaveRecipeClick(Sender: TObject);
var
  inputs: array of TWinControl;
begin
  inputs:= [
    edtRecipeName,
    dteRecipeCreation,
    edtPortionQtt,
    edtHomeMeasureQtt,
    edtHomeMeasureName
  ];

  if checkRequiredFields(inputs) then begin
    if (CurrentRecipe = nil) then CurrentRecipe:= TRecipe.Create;
    CurrentRecipe.name             := Trim(edtRecipeName.Text);
    CurrentRecipe.description      := Trim(mmRecipeDesc.Text);
    CurrentRecipe.creation         := dteRecipeCreation.Date;
    CurrentRecipe.modification     := dteRecipeModif.Date;
    CurrentRecipe.portion          := single(edtPortionQtt.Value);
    CurrentRecipe.domestic_measure := Trim(edtHomeMeasureName.Text);
    CurrentRecipe.total_portions   := single(edtHomeMeasureQtt.Value);
    CurrentRecipe.portion_unit     := 'g'; // TODO: combobox with 'g' and 'ml'

    pnTbsRegIngred.Caption:= 'Ingredientes: ' + CurrentRecipe.name;

    if (pnTbsRegRecipe.Caption = 'Editar receita') then
      fillRecipeList(CurrentRecipe.ingredients);

    tbsRegIngredient.Show;
  end;
end;

procedure TfrmMain.btnSearchAddIngredClick(Sender: TObject);
var
  query: string;
  tbcaHandler : TTBCAPageHandler;
begin
  query:= Trim(edtSearchIngred.Text);
  tbcaHandler:= TTBCAPageHandler.Create;
  try
    drawFoodList(tbcaHandler.searchFor(food, query));
    tbsSearchIngred.Show;
  finally
    tbcaHandler.Free;
  end;
end;

procedure TfrmMain.edtHomeMeasureQttKeyPress(Sender: TObject; var Key: char);
begin
    if (Key = '.') or (Key = ',') then Key:= #0;
end;

procedure TfrmMain.edtPortionQttKeyPress(Sender: TObject; var Key: char);
begin
  if (Key = '.') or (Key = ',') then Key:= #0;
end;

procedure TfrmMain.edtQtdIngredKeyPress(Sender: TObject; var Key: char);
begin
  if (Key = '.') or (Key = ',') then Key:= #0;
end;

procedure TfrmMain.btnCancelRecipeClick(Sender: TObject);
begin
  if (pnTbsRegRecipe.Caption = 'Editar receita') then begin
    tbsGenerateLabel.Show;
    pnTbsRegRecipe.Caption:= 'Cadastrar receita';
  end else
    tbsHome.Show;

  edtRecipeName.Clear;
  dteRecipeCreation.Clear;
  mmRecipeDesc.Text:='Descrição...';
  edtPortionQtt.Value:=0;
  edtHomeMeasureQtt.Value:=0;
  edtHomeMeasureName.Clear;
  edtSearchIngred.Clear;
  lvAddIngred.Items.Clear;
  pnTbsRegIngred.Caption:='Ingredientes';

  if not (CurrentRecipe = nil) then FreeAndNil(CurrentRecipe);
end;

procedure TfrmMain.btnChooseIngredClick(Sender: TObject);
begin
  ingredientToRecipeList;
end;

procedure TfrmMain.btnCopyNFTextClick(Sender: TObject);
begin
  mmTextNFLabel.SelectAll;
  mmTextNFLabel.CopyToClipboard;
  ShowMessage('Copiado para a área de transferência!');
  mmTextNFLabel.SelLength:=0;
end;

procedure TfrmMain.btnDelCancelRecipeClick(Sender: TObject);
var
  choosen: TListItem;
  confirm: TModalResult;
begin
  choosen:= lvChooseRecipe.Selected;

  if (choosen = nil) then
    ShowMessage('Primeiro selecione uma receita.')
  else begin
    confirm:= MessageDlg('Confirme a ação', 'Apagar a receita: ' + choosen.SubItems[0], mtConfirmation, [mbYes, mbNo], 'Após isso a receita sera apagado do banco de dados.');
    if (confirm = mrYes) then
      if TRecipe.exclude(choosen.Caption.ToInteger) then
        lvChooseRecipe.Items.Delete(choosen.Index);
  end;
end;

procedure TfrmMain.btnDeleteIngredientClick(Sender: TObject);
var
  selectedItem: TListItem;
  persist: boolean;
begin
  selectedItem:= lvAddIngred.Selected;
  persist:= pnTbsRegRecipe.Caption = 'Editar receita';

  if (selectedItem = nil) then begin
    ShowMessage('Selecione o item que quer apagar.');
  end else begin
    if CurrentRecipe.deleteIngredient(selectedItem.SubItems[1], persist) then
      lvAddIngred.Items.Delete(selectedItem.Index);
  end;
end;

procedure TfrmMain.btnEditSaveRecipeClick(Sender: TObject);
var
  choosen: TListItem;
begin
  choosen:= lvChooseRecipe.Selected;
  if (choosen = nil) then
    ShowMessage('Selecione uma receita para editar.')
  else begin
    CurrentRecipe:= TRecipe.Create(choosen.Caption.ToInteger);
    edtRecipeName.Text:= CurrentRecipe.name;
    dteRecipeCreation.Date:= CurrentRecipe.creation;
    dteRecipeModif.Date:= Now;
    mmRecipeDesc.Text:= CurrentRecipe.description;
    edtPortionQtt.Value:= CurrentRecipe.portion;
    edtHomeMeasureName.Text:= CurrentRecipe.domestic_measure;
    edtHomeMeasureQtt.Value:= CurrentRecipe.total_portions;
    pnTbsRegRecipe.Caption:= 'Editar receita';
    tbsRegRecipe.Show;
  end;
end;

procedure TfrmMain.btnPrintLabelClick(Sender: TObject);
var
  choosen: TListItem;
  dir, filename: string;
  selectedComponents: Array of Integer = (0, 1, 2, 2, 3, 4, 5, 6, 10, 11);
begin
  choosen:= lvChooseRecipe.Selected;

  if (choosen = nil) then begin
    ShowMessage('Selecione uma receita para gerar as informações nutricionais.');

  end else begin
    if not (CurrentRecipe = nil) then FreeAndNil(CurrentRecipe);
    if not (NFLabel = nil) then FreeAndNil(NFLabel);

    CurrentRecipe:= TRecipe.Create(StrToInt(choosen.Caption));
    NFLabel:= TNFLabel.Create(CurrentRecipe, selectedComponents);

    if rbLabelText.Checked then begin
      mmTextNFLabel.Text:= NFLabel.printLabel;
      pnTbsTextNFLabel.Caption:= CurrentRecipe.name;
      tbsTextNFLabel.Show;

    end else if rbLabelTable.Checked then begin
      SelectDirectory('Salvar arquivo em...', GetUserDir, dir);
      filename:= NFLabel.saveToFile(dir);
      OpenDocument(filename);
    end;
  end;
end;

procedure TfrmMain.btnPrintTicketClick(Sender: TObject);
var
  sql: string;
  item: TListViewDataItem;
begin
  if not (CurrentRecipe = nil) then FreeAndNil(CurrentRecipe);
  lvfChooseRecipe.Items.Clear;

  sql:= 'select * from Receita;';
  with dmMain do begin
    prepareNewSQL(query, sql);
    query.Open;

    lvChooseRecipe.BeginUpdate;
    repeat
      item.Data:= nil;
      SetLength(item.StringArray, 3);
      item.StringArray[0]:= Format('%.3d', [query.FieldByName('id').AsInteger]);
      item.StringArray[1]:= query.FieldByName('nome').AsString;
      item.StringArray[2]:= query.FieldByName('dt_criacao').AsString;
      lvfChooseRecipe.Items.Add(item);
      query.Next;
    until query.EOF;
    lvfChooseRecipe.InvalidateFilter;
    lvfChooseRecipe.ResetFilter;
    lvChooseRecipe.EndUpdate;
  end;
  tbsGenerateLabel.Show;
end;


procedure TfrmMain.btnCancelIngredClick(Sender: TObject);
begin
  tbsRegRecipe.Show;
  lvAddIngred.Items.Clear;
end;

procedure TfrmMain.btnBackIngredClick(Sender: TObject);
begin
  lvfChooseIngredient.Clear;
  edtQtdIngred.Text:='0';
  tbsRegIngredient.Show;
end;

procedure TfrmMain.btnBackToHomeClick(Sender: TObject);
begin
  tbsHome.Show;
end;

procedure TfrmMain.btnBackToRecipesClick(Sender: TObject);
begin
  tbsGenerateLabel.Show;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  updateGroupAndComponentData;
  tbsHome.Show;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  if not (CurrentRecipe = nil) then FreeAndNil(CurrentRecipe);
end;

procedure TfrmMain.lvAddIngredEdited(Sender: TObject; Item: TListItem; var AValue: string);
var
  idx, id: Integer;
  persist: Boolean;
  msg: string;
begin
  msg:= 'Ingrediente não encontrado em mémoria ou no banco de dados.';
  for idx:=0 to High(CurrentRecipe.ingredients) do begin
    if (CurrentRecipe.ingredients[idx].code = Item.SubItems[1]) then begin
      persist := pnTbsRegRecipe.Caption = 'Editar receita';
      id      := CurrentRecipe.ingredients[idx].id;
      if CurrentRecipe.updateIngredient(id, StrToFloat(AValue), persist) then begin
        msg:=       'Ingrediente: ';
        msg:= msg + CurrentRecipe.ingredients[idx].name + sLineBreak;
        msg:= msg + 'Quantidade: ';
        msg:= msg + CurrentRecipe.ingredients[idx].quantity.ToString;
      end else begin
        msg:= 'Ocorreu um erro ao atualizar o ingrediente.';
      end;
      break;
    end;
  end;
  ShowMessage(msg);
end;

procedure TfrmMain.lvChooseIngredientDblClick(Sender: TObject);
begin
  ingredientToRecipeList;
end;

procedure TfrmMain.drawFoodList(foodList: TStringList);
var
  c: integer;
  item: TListViewDataItem;
begin
  c := 0;
  lvfChooseIngredient.Items.Clear;
  lvChooseIngredient.BeginUpdate;
  while (c < foodList.Count) do
  begin
    item.Data:=nil;
    SetLength(item.StringArray, 3);
    item.StringArray[0]:=foodList.Strings[c];
    item.StringArray[1]:=foodList.Strings[c + 1];
    item.StringArray[2]:=foodList.Strings[c + 2];
    lvfChooseIngredient.Items.Add(item);
    c := c + 3;
  end;
  lvfChooseIngredient.InvalidateFilter;
  lvfChooseIngredient.ResetFilter;
  lvChooseIngredient.EndUpdate;
end;

procedure TfrmMain.ingredientToRecipeList;
var
  len: integer;
  ing: TIngredient;
  tbca: TTBCAPageHandler;
  persist: Boolean;
  choosen, item: TListItem;
begin
  choosen:= lvChooseIngredient.Selected;

  if not (choosen = nil) then begin
    if not (edtQtdIngred.Value <= 0) then begin
      tbca         := TTBCAPageHandler.Create;
      ing          := TIngredient.Create;
      ing.code     := choosen.Caption;
      ing.name     := choosen.SubItems[0];
      ing.group    := choosen.SubItems[1];
      ing.quantity := edtQtdIngred.Value;
      ing.creation := Now;
      ing.components:= tbca.searchComponents(ing.code);
      FreeAndNil(tbca);

      if ing.save then begin
        persist:= pnTbsRegRecipe.Caption = 'Editar receita';
        CurrentRecipe.addIngredient(ing, persist);

        len:= lvAddIngred.Items.Count;
        item:= lvAddIngred.Items.Add;
        item.Caption:= FormatFloat('0.000',  ing.quantity);
        item.SubItems.Add(ing.name);
        item.SubItems.Add(ing.code);
        lvfChooseIngredient.Clear;
        edtQtdIngred.Text:='0';
        tbsRegIngredient.Show;
      end else ShowMessage('Falha ao tentar salvar o ingrediente.');
    end else ShowMessage('Indique a quantidade do ingrediente na sua receita.');
  end else ShowMessage('Escolha um ingrediente para incluir na receita.');
end;

procedure TfrmMain.fillRecipeList(ingredients: TIngredients);
var
  idx: integer;
  item: TListItem;
begin
  for idx:=0 to High(ingredients) do begin
    item:= lvAddIngred.Items.Add;
    item.Caption:= FormatFloat('0.000', ingredients[idx].quantity);
    item.SubItems.Add(ingredients[idx].name);
    item.SubItems.Add(ingredients[idx].code);
  end;
end;

procedure TfrmMain.updateGroupAndComponentData;
var
  in75: TIN75PageHandler;
  tbca: TTBCAPageHandler;
  isConnected, needUpdate: Boolean;
begin
  in75:= TIN75PageHandler.Create;
  tbca:= TTBCAPageHandler.Create;

  isConnected:= in75.hasConnectivity and tbca.hasConnectivity;
  needUpdate:= in75.dailyValuesDataIsOld;

  if isConnected and needUpdate then begin
    try
      in75.updateDailyValuesData;
      tbca.updateGroupData;
    finally
      in75.Free;
      tbca.Free;
      in75:= nil;
      tbca:= nil;
    end;
  end;
end;

function TfrmMain.checkRequiredFields(fields: array of TWinControl): Boolean;
var
  c, n: integer;
  value: string;
  i: double;
begin
  n:= Length(fields);

  for c:=0 to n - 1 do begin
    case fields[c].ClassName of
      'TEdit': begin
        value:= Trim(TEdit(fields[c]).Text);
        if value.Length < 1 then value:= '';
      end;
      'TFloatSpinEdit': begin
        i:= TFloatSpinEdit(fields[c]).Value;
        if i = 0 then value:= '';
      end;
      'TDateEdit': begin
        value:= Trim(TDateEdit(fields[c]).Text);
        if value = '-  -' then value:= '';
      end;
    end;

    if value.IsEmpty then begin
      ShowMessage('Preencha os campos obrigatórios.');
      case fields[c].ClassName of
        'TEdit': begin
          TEdit(fields[c]).Clear;
          TEdit(fields[c]).SetFocus;
        end;
        'TFloatSpinEdit': begin
          TFloatSpinEdit(fields[c]).Clear;
          TFloatSpinEdit(fields[c]).SetFocus;
        end;
        'TDateEdit': begin
          TDateEdit(fields[c]).Clear;
          TDateEdit(fields[c]).SetFocus;
        end;
      end;
      Exit(false);
    end;
  end;
  result:= True;
end;

procedure TfrmMain.clearTextFields(fields: array of TWinControl);
var
  c, n: integer;
begin
  n:= Length(fields);

  for c:=0 to n - 1 do begin
    case fields[c].ClassName of
      'TEdit': TEdit(fields[c]).Clear;
      'TFloatSpinEdit': TFloatSpinEdit(fields[c]).Text:='0';
      'TDateEdit': TDateEdit(fields[c]).Clear;
      'TMemo': TMemo(fields[c]).Text:= 'Descrição';
    end;
  end;
end;

end.

