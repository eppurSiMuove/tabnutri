unit uNFLabel;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLdb,
  fpSpreadsheet, fpsTypes, laz_fpspreadsheet, fpsallformats,

  uDMMain, uRecipe, uComponent;

type

  TNFRowArray = Array[0..3] of Array of string;

  { TNFLabel }

  TNFLabel = Class
    private
      FComps: TNFComponents;
      FSelComps: Array of Integer;
      FRecipe: TRecipe;

      function getComps: TNFComponents;
      function getRecipe: TRecipe;
      procedure setComps(AValue: TNFComponents);
      procedure setRecipe(AValue: TRecipe);

      function calcNutritionFacts: TNFComponents;
      function nutritionFactsTable: TNFRowArray;

    public
      constructor Create(ARecipe: TRecipe; selectedComponents: Array of Integer);
      property recipe: TRecipe read getRecipe write setRecipe;
      property components: TNFComponents read getComps write setComps;
      function printLabel: string;
      function saveToFile(directory: string): string;
  end;

implementation

{ TNFLabel }

function TNFLabel.getComps: TNFComponents;
begin
  result:= FComps;
end;

function TNFLabel.getRecipe: TRecipe;
begin
  result:= FRecipe;
end;

procedure TNFLabel.setRecipe(AValue: TRecipe);
begin
  if FRecipe = AValue then Exit;
  FRecipe:= AValue;
end;

procedure TNFLabel.setComps(AValue: TNFComponents);
begin
  if FComps = AValue then Exit;
  FComps:= AValue;
end;

function TNFLabel.calcNutritionFacts: TNFComponents;
var
  compList: TNFComponents;
  component: TNFComponent;
  c: integer;
  sql: string;
  auxQuery: TSQLQuery;
  q100r, qpr, p100r, ppr: single;
begin
  sql:= 'select * from Componente;';
  dmMain.newQuery(auxQuery);
  dmMain.prepareNewSQL(auxQuery, sql);
  auxQuery.Open;
  auxQuery.Last;
  compList:= TNFComponents.Create;
  SetLength(compList, auxQuery.RecordCount);
  auxQuery.First;

  c:= 0;
  repeat
    component:= TNFComponent.Create;
    component.id:= auxQuery.FieldByName('id').AsInteger;
    component.name:= auxQuery.FieldByName('nome').AsString;
    component.creation:= StrToDate(auxQuery.FieldByName('dt_reg').AsString, 'yyyy-mm-dd');
    component.unitMeasurement:= auxQuery.FieldByName('unidade').AsString;
    component.rdv:= auxQuery.FieldByName('vdr').AsFloat;
    compList[c]:= component;
    auxQuery.Next;
    c:= c + 1;
  until auxQuery.EOF;

  for c:=0 to High(compList) do begin
    q100r:= Self.recipe.getComponentMassByPortion(c + 1, 100);
    p100r:= (100 * q100r) / compList[c].rdv;

    qpr:= Self.recipe.getComponentMassByPortion(c + 1, Self.recipe.portion);
    ppr:= (100 * qpr) / compList[c].rdv;

    compList[c].qtty100Rec        := q100r;
    compList[c].qttyPortionRec    := qpr;
    compList[c].percent100Rec     := p100r;
    compList[c].percentPortionRec := ppr;
  end;

  result:= compList;
end;

function TNFLabel.nutritionFactsTable: TNFRowArray;
const
  s: string = '%s (%s)';
  n: string = '%n';
var
  i, c, idx: integer;
  nftable: TNFRowArray;
  name, unitMeasurement: string;
  qtty100Rec, qttyPortionRec, percentPortionRec: single;
begin
  SetLength(nftable[0], Length(Self.components));
  SetLength(nftable[1], Length(Self.components));
  SetLength(nftable[2], Length(Self.components));
  SetLength(nftable[3], Length(Self.components));

  c:= 0;
  for i:=0 to High(FSelComps) do begin
    idx:= FSelComps[i];

    name:= Self.components[idx].name;
    if ((idx = 2) or (idx = 5) or (idx = 6)) then begin
      if (idx = 2) then begin
        if (c = 0) then begin
          name:= 'Açúcares totais';
        end;
        c:= c + 1;
        if (c = 2) then
          name:= '  ' + name;
      end;
      name:= '  ' + name;
    end;

    qtty100Rec:= Self.components[idx].qtty100Rec;
    qttyPortionRec:= Self.components[idx].qttyPortionRec;
    percentPortionRec:= Self.components[idx].percentPortionRec;
    unitMeasurement:= components[idx].unitMeasurement;

    nftable[0][i]:= Format(s, [name, unitMeasurement]);
    nftable[1][i]:= FormatFloat('0.#', qtty100Rec);
    nftable[2][i]:= FormatFloat('0.#', qttyPortionRec);
    nftable[3][i]:= FormatFloat('0.#', percentPortionRec);
  end;

  result:= nftable;
end;

constructor TNFLabel.Create(ARecipe: TRecipe; selectedComponents: array of Integer);
var
  i, n: integer;
begin
  inherited Create;

  Self.recipe:= ARecipe;
  Self.components:= calcNutritionFacts;

  n:= Length(selectedComponents);
  SetLength(FSelComps, n);
  for i:=0 to n - 1 do
    FSelComps[i]:= selectedComponents[i];
end;

function TNFLabel.printLabel: string;
var
  str: string;
begin
  str:= 'INFORMAÇÃO NUTRICIONAL' + LineEnding +
        'Porções por embalagem: %n ▪ Porção: %n g (%s)' + LineEnding +
        'Por 100 g (%n g, %%VD*): ' +
        'Valor energético %n kcal (%n kcal, %n%%) ▪ ' +
        'Carboidratos %n g (%n g, %n%%), ' +
        'dos quais Açúcares totais %n g (%n g, %n%%), '+
        'Açúcares adicionados %n g (%n g, %n%%) ▪ ' +
        'Proteínas %n g (%n g, %n%%) ▪ ' +
        'Gorduras totais %n g (%n g, %n%%), ' +
        'das quais Gorduras saturadas %n g (%n g, %n%%), ' +
        'Gorduras trans %n g (%n g, %n%%) ▪ ' +
        'Fibras alimentares %n g (%n g, %n%%) ▪ ' +
        'Sódio %n mg (%n g, %n%%). ' +
        '*Percentual de valores diários fornecidos pela porção.';

  result:= Format(str, [
    Self.recipe.total_portions,
    Self.recipe.portion,
    Self.recipe.domestic_measure,
    Self.recipe.portion,
    Self.components[0].qtty100Rec, Self.components[0].qttyPortionRec, Self.components[0].percentPortionRec,
    Self.components[1].qtty100Rec, Self.components[1].qttyPortionRec, Self.components[1].percentPortionRec,
    Self.components[2].qtty100Rec, Self.components[2].qttyPortionRec, Self.components[2].percentPortionRec,
    Self.components[2].qtty100Rec, Self.components[2].qttyPortionRec, Self.components[2].percentPortionRec,
    Self.components[3].qtty100Rec, Self.components[3].qttyPortionRec, Self.components[3].percentPortionRec,
    Self.components[4].qtty100Rec, Self.components[4].qttyPortionRec, Self.components[4].percentPortionRec,
    Self.components[5].qtty100Rec, Self.components[5].qttyPortionRec, Self.components[5].percentPortionRec,
    Self.components[6].qtty100Rec, Self.components[6].qttyPortionRec, Self.components[6].percentPortionRec,
    Self.components[10].qtty100Rec, Self.components[10].qttyPortionRec, Self.components[10].percentPortionRec,
    Self.components[11].qtty100Rec, Self.components[11].qttyPortionRec, Self.components[11].percentPortionRec
  ]);
end;

function TNFLabel.saveToFile(directory: string): string;
var
  i, k: integer;
  cell: PCell;
  workbook: TsWorkbook;
  worksheet: TsWorksheet;
  tableArrows: TNFRowArray;
  measureHeader: Array[0..2] of String;
  filename, portionQtty, portionHeader: string;
begin
  filename:= Self.recipe.id.ToString + '.' + Self.recipe.name.Replace(' ', '-');
  filename:= directory + DirectorySeparator + filename.ToLower + STR_OPENDOCUMENT_CALC_EXTENSION;

  if FileExists(filename) then
    DeleteFile(filename);

  workbook:= TsWorkbook.Create;
  try
    worksheet:= workbook.AddWorksheet('Tabela Nutricional');

    with worksheet do begin
      MergeCells(1, 2, 1, 5);
      WriteRowHeight(1, 0.55, suCentimeters);
      cell:= WriteText(1, 2, 'INFORMAÇÃO NUTRICIONAL');
      WriteFontSize(cell, 10);
      WriteFontStyle(cell, [fssBold]);
      WriteHorAlignment(cell, haCenter);
      WriteVertAlignment(cell, vaCenter);
      WriteBorders(cell, [cbNorth, cbSouth]);
      WriteBorderStyle(cell, cbNorth, lsThin, scBlack);
      WriteBorderStyle(cell, cbSouth, lsThin, scBlack);

      MergeCells(2, 2, 2, 5);
      WriteRowHeight(2, 0, suCentimeters);
      cell:= WriteText(2, 2, 'Porções por embalagem: 000');
      WriteVertAlignment(cell, vaCenter);

      portionQtty:= Self.recipe.portion.ToString +' '+ Self.recipe.portion_unit;

      portionHeader:= 'Porção: ' + portionQtty +
                      ' (' + Self.recipe.domestic_measure + ')';

      MergeCells(3, 2, 3, 5);
      WriteRowHeight(3, 0.55, suCentimeters);
      cell:= WriteText(3, 2, portionHeader);
      WriteBorders(cell, [cbSouth]);
      WriteVertAlignment(cell, vaCenter);
      WriteBorderStyle(cell, cbSouth, lsThick, scBlack);

      measureHeader[0]:= '100 g';
      measureHeader[1]:= portionQtty;
      measureHeader[2]:= '%VD*';

      for i:=3 to 5 do begin
        WriteColWidth(i, 7, suChars);
        cell:= WriteText(5, i, measureHeader[i - 3]);
        WriteVertAlignment(cell, vaCenter);
        WriteHorAlignment(cell, haCenter);
        WriteFontStyle(cell, [fssBold]);
        WriteBorders(cell, [cbWest]);
        WriteBorderStyle(cell, cbWest, lsThin, scBlack);
      end;

      tableArrows:= nutritionFactsTable;

      for i:=6 to 15 do begin
        cell:= WriteText(i, 2, tableArrows[0][i - 6]);
        WriteVertAlignment(cell, vaCenter);
        WriteFontSize(cell, 8);
        WriteBorders(cell, [cbNorth]);
        WriteBorderStyle(cell, cbNorth, lsThin, scBlack);
        for k:=3 to 5 do begin
          cell:= WriteText(i, k, tableArrows[k - 2][i - 6]);
          WriteVertAlignment(cell, vaCenter);
          WriteHorAlignment(cell, haCenter);
          WriteFontSize(cell, 8);
          WriteBorders(cell, [cbWest, cbNorth]);
          WriteBorderStyle(cell, cbWest, lsThin, scBlack);
          WriteBorderStyle(cell, cbNorth, lsThin, scBlack);
        end;
      end;

      MergeCells(16, 2, 16, 5);
      cell:= WriteText(16, 2, '*Percentual de valores diários fornecidos pela porção.');
      WriteBorders(cell, [cbNorth, cbSouth]);
      WriteVertAlignment(cell, vaCenter);
      WriteBorderStyle(cell, cbNorth, lsMedium, scBlack);
      WriteBorderStyle(cell, cbSouth, lsThin, scBlack);
      WriteFontSize(cell, 6);

      WriteColWidth(1, 0.1, suCentimeters);
      WriteColWidth(2, 22, suChars);
      WriteColWidth(6, 0.1, suCentimeters);
      WriteRowHeight(4, 0.1, suCentimeters);

      MergeCells(1, 1, 16, 1);
      cell:= GetCell(1, 1);
      WriteBorders(cell, [cbSouth, cbWest, cbNorth]);
      WriteBorderStyle(cell, cbSouth, lsThin, scBlack);
      WriteBorderStyle(cell, cbWest, lsThin, scBlack);
      WriteBorderStyle(cell, cbNorth, lsThin, scBlack);

      MergeCells(1, 6, 16, 6);
      cell:= GetCell(1, 6);
      WriteBorders(cell, [cbSouth, cbEast, cbNorth]);
      WriteBorderStyle(cell, cbSouth, lsThin, scBlack);
      WriteBorderStyle(cell, cbEast, lsThin, scBlack);
      WriteBorderStyle(cell, cbNorth, lsThin, scBlack);
    end;

    workbook.WriteToFile(filename, sfOpenDocument);
    result:= filename;
  finally
    workbook.Free;
  end;
end;

end.

