unit uPageHandler;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, FPHTTPClient;

type

  { TPageHandler }

  TPageHandler = class
    private
      function getMainURL: string;
    protected
      FMainURL: string;
      FHttpClient: TFPHTTPClient;
    public
      constructor Create;
      destructor Destroy; override;
      property mainURL: string read getMainURL;
      function hasConnectivity: Boolean; inline;
  end;

implementation

{ TPageHandler }

function TPageHandler.getMainURL: string;
begin
  result:= FMainURL;
end;

function TPageHandler.hasConnectivity: Boolean;
begin
  result:= false;
  try
    Self.FHttpClient.HTTPMethod('HEAD', Self.mainURL, nil, [200]);
    result:= true;
  except
    result:= false;
  end;
end;

constructor TPageHandler.Create;
begin
  inherited Create;
  FHttpClient:= TFPHTTPClient.Create(nil);
  FHttpClient.AllowRedirect:= true;
end;

destructor TPageHandler.Destroy;
begin
  FHttpClient.Free;
  FHttpClient:= nil;
  inherited Destroy;
end;

end.

