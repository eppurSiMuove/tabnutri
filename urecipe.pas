unit uRecipe;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, uDMMain, uIngredient;

type

  { TRecipe }

  TRecipe = class
    private
      FID: integer;
      FDesc: string;
      FMedCas: string;
      FModif: TDateTime;
      FName: string;
      FPortion: single;
      FCreation: TDateTime;
      FTPortion: single;
      FUnPortion: string;
      FIngreds: TIngredients;
      function getDesc: string;
      function getID: integer;
      function getIngreds: TIngredients;
      function getDMeasure: string;
      function getModif: TDateTime;
      function getName: string;
      function getPortion: single;
      function getCreation: TDateTime;
      function getTPortion: single;
      function getPortionUnit: string;
      procedure setDesc(AValue: string);
      procedure setID(AValue: integer);
      procedure setIngreds(AValue: TIngredients);
      procedure setDMeasure(AValue: string);
      procedure setModif(AValue: TDateTime);
      procedure setName(AValue: string);
      procedure setPortion(AValue: single);
      procedure setCreation(AValue: TDateTime);
      procedure setTPortion(AValue: single);
      procedure setPortionUnit(AValue: string);
    public
      property id: integer read getID write setID;
      property name: string read getName write setName;
      property description: string read getDesc write setDesc;
      property portion: single read getPortion write setPortion;
      property portion_unit: string read getPortionUnit write setPortionUnit;
      property domestic_measure: string read getDMeasure write setDMeasure;
      property total_portions: single read getTPortion write setTPortion;
      property creation: TDateTime read getCreation write setCreation;
      property modification: TDateTime read getModif write setModif;
      property ingredients: TIngredients read getIngreds write setIngreds;
      function getComponentMass(AComponentID: integer): single;
      function getTotalMass: single;
      function getComponentMassByPortion(AComponentID: integer; APortion: single): single;
      function save: Boolean;
      function update: Boolean;
      function addIngredient(AIngredient: TIngredient; OPersistent: Boolean = false): Boolean;
      function updateIngredient(AIngredientID: integer; AQuantity: single; OPersistent: Boolean = false): Boolean;
      function deleteIngredient(AIngredientCode: string; OPersistent: Boolean = false): Boolean;
      function saveIngredients: Boolean;
      class function exclude(ARecipeID: integer): Boolean; static;
      constructor Create; overload;
      constructor Create(ARecipeID: integer); overload;
  end;

  TRecipes = array of TRecipe;

implementation

{ TRecipe }

function TRecipe.getDesc: string;
begin
  result:= FDesc;
end;

function TRecipe.getID: integer;
begin
  result:= FID;
end;

function TRecipe.getIngreds: TIngredients;
begin
  result:= FIngreds;
end;

function TRecipe.getDMeasure: string;
begin
  result:= FMedCas;
end;

function TRecipe.getModif: TDateTime;
begin
  result:= FModif;
end;

function TRecipe.getName: string;
begin
  result:= FName;
end;

function TRecipe.getPortion: single;
begin
  result:= FPortion;
end;

function TRecipe.getCreation: TDateTime;
begin
  result:= FCreation;
end;

function TRecipe.getTPortion: single;
begin
  result:= FTPortion;
end;

function TRecipe.getPortionUnit: string;
begin
  result:= FUnPortion;
end;

procedure TRecipe.setDesc(AValue: string);
begin
  if AValue = FDesc then Exit;
  FDesc:= AValue;
end;

procedure TRecipe.setID(AValue: integer);
begin
  if AValue = FID then Exit;
  FID:= AValue;
end;

procedure TRecipe.setIngreds(AValue: TIngredients);
begin
  if AValue = FIngreds then Exit;
  FIngreds:= AValue;
end;

procedure TRecipe.setDMeasure(AValue: string);
begin
  if AValue = FMedCas then Exit;
  FMedCas:= AValue;
end;

procedure TRecipe.setModif(AValue: TDateTime);
begin
  if AValue = FModif then Exit;
  FModif:= AValue;
end;

procedure TRecipe.setName(AValue: string);
begin
  if AValue = FName then Exit;
  FName:= AValue;
end;

procedure TRecipe.setPortion(AValue: single);
begin
  if AValue = FPortion then Exit;
  FPortion:= AValue;
end;

procedure TRecipe.setCreation(AValue: TDateTime);
begin
  if AValue = FCreation then Exit;
  FCreation:= AValue;
end;

procedure TRecipe.setTPortion(AValue: single);
begin
  if AValue = FTPortion then Exit;
  FTPortion:= AValue;
end;

procedure TRecipe.setPortionUnit(AValue: string);
begin
  if AValue = FUnPortion then Exit;
  FUnPortion:= AValue;
end;

function TRecipe.getComponentMass(AComponentID: integer): single;
var
  ingredient: TIngredient;
begin
  result:= 0;
  for ingredient in Self.ingredients do begin
    result:= result + ingredient.getComponentMass(AComponentID);
  end;
end;

function TRecipe.getTotalMass: single;
var
  ingredient: TIngredient;
begin
  result:= 0;
  for ingredient in Self.ingredients do begin
    result:= result + ingredient.quantity;
  end;
end;

function TRecipe.getComponentMassByPortion(AComponentID: integer; APortion: single): single;
begin
  result:= (Self.getComponentMass(AComponentID) * APortion) / Self.getTotalMass;
end;

function TRecipe.save: Boolean;
var
  sql: string;
begin
  sql:=       'insert into Receita ';
  sql:= sql + '(nome, descricao, tamanho_porcao, un_porcao, medida_cas, porcoes_cas, dt_criacao, dt_modif) ';
  sql:= sql + 'values ';
  sql:= sql + '(:name, :description, :portion, :portion_unit, :domestic_measure, :total_portions, :creation, :modification);';

  try
    with dmMain do begin
      prepareNewSQL(query, sql);
      query.ParamByName('name').AsString             := self.name;
      query.ParamByName('description').AsString      := self.description;
      query.ParamByName('portion').AsFloat           := self.portion;
      query.ParamByName('portion_unit').AsString     := self.portion_unit;
      query.ParamByName('domestic_measure').AsString := self.domestic_measure;
      query.ParamByName('total_portions').AsFloat    := self.total_portions;
      query.ParamByName('creation').AsString         := FormatDateTime('yyyy-mm-dd', self.creation);
      query.ParamByName('modification').AsString     := FormatDateTime('yyyy-mm-dd', self.modification);
      query.ExecSQL;
      transaction.Commit;

      prepareNewSQL(query, 'select last_insert_rowid() as last_id;');
      query.Open;
      self.id:= query.FieldByName('last_id').AsInteger;
    end;
    result:= true;
  except
    result:= false;
  end;
end;

function TRecipe.update: Boolean;
var
  sql: string;
begin
  sql:=       'update Receita set ';
  sql:= sql + '(nome, descricao, tamanho_porcao, un_porcao, medida_cas, porcoes_cas, dt_criacao, dt_modif) ';
  sql:= sql + '= ';
  sql:= sql + '(:name, :description, :portion, :portion_unit, :domestic_measure, :total_portions, :creation, :modification) ';
  sql:= sql + 'where id = :id;';
  try
    with dmMain do begin
      prepareNewSQL(query, sql);
      query.ParamByName('id').AsInteger              := self.id;
      query.ParamByName('name').AsString             := self.name;
      query.ParamByName('description').AsString      := self.description;
      query.ParamByName('portion').AsFloat           := self.portion;
      query.ParamByName('portion_unit').AsString     := self.portion_unit;
      query.ParamByName('domestic_measure').AsString := self.domestic_measure;
      query.ParamByName('total_portions').AsFloat    := self.total_portions;
      query.ParamByName('creation').AsString         := FormatDateTime('yyyy-mm-dd', self.creation);
      query.ParamByName('modification').AsString     := FormatDateTime('yyyy-mm-dd', self.modification);
      query.ExecSQL;
      transaction.Commit;
    end;
    result:= true;
  except
    result:= false;
  end;
end;

function TRecipe.addIngredient(AIngredient: TIngredient; OPersistent: Boolean): Boolean;
var
  sql: string;
begin
  result:= true;
  try
    SetLength(FIngreds, Length(FIngreds) + 1);
    Self.ingredients[Length(FIngreds)-1]:= AIngredient;

    if OPersistent then begin
      sql:= 'insert into IngredRec ( id_ingred,  id_receit,  quantidade) ';
      sql:= sql +          'values (:id_ingred, :id_receit, :quantidade);';
      with dmMain do begin
        prepareNewSQL(query, sql);
        query.ParamByName('id_ingred').AsInteger:= AIngredient.id;
        query.ParamByName('id_receit').AsInteger:= Self.id;
        query.ParamByName('quantidade').AsFloat:= AIngredient.quantity;
        query.ExecSQL;
        transaction.Commit;
      end;
    end;
  except
    Result:= false;
  end;
end;

function TRecipe.updateIngredient(AIngredientID: integer; AQuantity: single; OPersistent: Boolean): Boolean;
var
  idx: integer;
  sql: string;
begin
  result:= false;
  for idx:=0 to High(Self.ingredients) do begin
    if (Self.ingredients[idx].id = AIngredientID) then begin
      Self.ingredients[idx].quantity:= AQuantity;
      if OPersistent then begin
        sql:=       'update IngredRec set quantidade = :quantidade ';
        sql:= sql + 'where id_ingred = :id_ingred and id_receit = :id_receit;';
        with dmMain do begin
          prepareNewSQL(query, sql);
          query.ParamByName('id_ingred').AsInteger:= AIngredientID;
          query.ParamByName('id_receit').AsInteger:= Self.id;
          query.ParamByName('quantidade').AsFloat:= AQuantity;
          query.ExecSQL;
          transaction.Commit;
        end;
      end;
      result:= true;
      break;
    end;
  end;
end;

function TRecipe.deleteIngredient(AIngredientCode: string; OPersistent: Boolean): Boolean;
var
  idx: integer;
  sql: string;
begin
  Result:= False;
  for idx:=0 to High(FIngreds) do begin
    if (FIngreds[idx].code = AIngredientCode) then begin
      if OPersistent then begin
        sql:= 'delete from IngredRec where ';
        sql:= sql + 'id_ingred = :id_ingred and id_receit = :id_receit;';
        with dmMain do begin
          prepareNewSQL(qryAux, sql);
          qryAux.ParamByName('id_ingred').AsInteger:= FIngreds[idx].id;
          qryAux.ParamByName('id_receit').AsInteger:= Self.id;
          qryAux.ExecSQL;
          transaction.Commit;
        end;
      end;
      Delete(FIngreds, idx, 1);
      Result:= True;
      break;
    end;
  end;
end;

function TRecipe.saveIngredients: Boolean;
var
  sql: string;
  c: integer;
begin
  sql:= 'insert into IngredRec ( id_ingred,  id_receit,  quantidade) ';
  sql:= sql +          'values (:id_ingred, :id_receit, :quantidade);';

  for c:=0 to High(Self.ingredients) do begin
    try
      with dmMain do begin
        prepareNewSQL(query, sql);
        query.ParamByName('id_ingred').AsInteger:= Self.ingredients[c].id;
        query.ParamByName('id_receit').AsInteger:= Self.id;
        query.ParamByName('quantidade').AsFloat:= Self.ingredients[c].quantity;
        query.ExecSQL;
      end;
    except
      on E: Exception do begin
        dmMain.transaction.Rollback;
        Exit(false);
      end;
    end;
  end;

  dmMain.transaction.Commit;
  Exit(true);
end;

class function TRecipe.exclude(ARecipeID: integer): Boolean;
const
    sql = 'delete from Receita where id = :id;';
begin
  with dmMain do begin
    prepareNewSQL(query, sql);
    query.ParamByName('id').AsInteger:= ARecipeID;
    try
      query.ExecSQL;
      transaction.Commit;
    except
      on E: Exception do begin
        transaction.Rollback;
        Exit(false);
      end;
    end;
  end;
end;

constructor TRecipe.Create;
begin
  inherited Create;
end;

constructor TRecipe.Create(ARecipeID: integer);
var
  sql: string;
  ingredient: TIngredient;
  c, id_ing: integer;
  qt_ing: single;
begin
  inherited Create;

  with dmMain do begin

    sql:= 'select * from Receita where id = :id;';
    prepareNewSQL(query, sql);
    query.ParamByName('id').AsInteger:= ARecipeID;
    query.Open;

    Self.id:=               query.FieldByName('id').AsInteger;
    Self.name:=             query.FieldByName('nome').AsString;
    Self.description:=      query.FieldByName('descricao').AsString;
    Self.portion:=          query.FieldByName('tamanho_porcao').AsFloat;
    Self.portion_unit:=     query.FieldByName('un_porcao').AsString;
    Self.domestic_measure:= query.FieldByName('medida_cas').AsString;
    Self.total_portions:=   query.FieldByName('porcoes_cas').AsFloat;
    Self.creation:=         StrToDate(query.FieldByName('dt_criacao').AsString, 'yyyy-mm-dd');
    Self.modification:=     StrToDate(query.FieldByName('dt_modif').AsString, 'yyyy-mm-dd');

    sql:= 'select id_ingred, quantidade from IngredRec where id_receit = :id_receit;';
    prepareNewSQL(query, sql);
    query.ParamByName('id_receit').AsInteger:= ARecipeID;
    query.Open;
    query.Last;
    SetLength(FIngreds, query.RecordCount);
    query.First;

    c:= 0;
    repeat
      id_ing:= query.FieldByName('id_ingred').AsInteger;
      qt_ing:= query.FieldByName('quantidade').AsFloat;
      ingredient:= TIngredient.Create(Self.id, id_ing, qt_ing, Self.portion);
      Self.ingredients[c]:= ingredient;
      query.Next;
      c:= c + 1;
    until query.EOF;
  end;
end;

end.

