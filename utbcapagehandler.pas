unit uTBCAPageHandler;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, OpenSSLSockets, SAX_HTML, DOM_HTML, DOM,

  uPageHandler, uDMMain, uComponent;

type

  TSearchSubject = (food, component, group);

  { TTBCAPageHandler }

  TTBCAPageHandler = class(TPageHandler)
    private
      foodAndGroupURL: string;
      componentURL: string;
      function rawFoodPage(AStr: string): string;
      function rawCompPage(AStr: string): string;
      function rawGrpPage: string;
      function listFoods(ARawHtml: string): TStringList;
      function listGrps(ARawHtml: string): TStringList;
      function listComps(ARawHtml: string): TStringList;
      function hasIN75Name(var ATBCAComponent: string): Boolean;
    public
      constructor Create;
      function searchFor(ASubject: TSearchSubject; ASearch: string): TStringList;
      function searchComponents(AIngredientCode: string): TNFComponents;
      function updateGroupData: Boolean;
  end;

implementation

{ TTBCAPageHandler }

function TTBCAPageHandler.searchFor(ASubject: TSearchSubject; ASearch: string): TStringList;
var
  rawPage: string;
begin
  case ASubject of

    food: begin
      rawPage := rawFoodPage(ASearch);
      result  := listFoods(rawPage);   // [ cód, ing, grp, ... ]
    end;

    component: begin
      rawPage := rawCompPage(ASearch);
      result  := listComps(rawPage);   // [ comp, unid, val, ... ]
    end;

    group: begin
      rawPage := rawGrpPage;
      result  := listGrps(rawPage);    // [ grp, val, ... ]
    end;
  end;
end;

function TTBCAPageHandler.searchComponents(AIngredientCode: string): TNFComponents;
var
  c, i, l: integer;
  list: TStringList;
  components: TNFComponents;
  comp: uComponent.TNFComponent;
begin
  list:= TStringList.Create;
  list:= searchFor(component, AIngredientCode);
  components:= TNFComponents.Create;

  c:= 0;
  i:= 0;
  l:= trunc(list.Count / 3);

  try
    SetLength(components, l);
    while (c < list.Count) do begin
      comp:= uComponent.TNFComponent.Create;
      comp.name:= list[c];
      comp.unitMeasurement:= list[c + 1];
      comp.quantity:= StrToFloat(list[c + 2]);
      comp.creation:= Now;
      components[i]:= comp;
      c:= c + 3;
      inc(i);
    end;
    result:= components;
  except
    on E: Exception do begin
      FreeAndNil(comp);
      raise E;
      Exit;
    end;
  end;
end;

function TTBCAPageHandler.updateGroupData: Boolean;
var
  data: TStringList;
  sql: string;
  c: integer;
begin
  sql:= 'delete from Grupo;';
  with dmMain do begin
    prepareNewSQL(query, sql);
    query.ExecSQL;
  end;

  data:= TStringList.Create;
  data:= searchFor(group, '');

  sql:= 'insert into Grupo (nome, valor) values (:name, :value)';

  c:=0;
  while (c < data.Count) do begin
    try
      with dmMain do begin
        prepareNewSQL(query, sql);
        query.ParamByName('name').AsString   := data[c];
        query.ParamByName('value').AsInteger := StrToInt(data[c + 1]);
        query.ExecSQL;
        c:= c + 2;
      end;
    except
      on E: Exception do begin
        dmMain.transaction.Rollback;
        result:= false;
        raise E;
      end;
    end;
  end;
  dmMain.transaction.Commit;
  result:= true;
end;

function TTBCAPageHandler.rawFoodPage(AStr: string): string;
var
  PostData: TStringList;
begin
  PostData:= TStringList.Create;
  PostData.Add('produto=' + AStr);
  PostData.Add('cmb_tipo_alimento=');
  PostData.Add('guarda=tomo1');
  PostData.Add('cmb_grupo=');
  try
    result:= Self.FHttpClient.SimpleFormPost(foodAndGroupURL, PostData);
  except
    on E: Exception do begin
      raise E;
      Exit;
    end;
  end;
end;

function TTBCAPageHandler.rawGrpPage: string;
var
  page: string;
begin
  try
    page:= Self.FHttpClient.Get(foodAndGroupURL);
    result:= page;
  except
    on E: Exception do begin
      raise E;
      Exit;
    end;
  end;
end;

function TTBCAPageHandler.rawCompPage(AStr: string): string;
var
  page: string;
begin
  try
    page:= Self.FHttpClient.Get(componentURL + AStr);
    result:= page;
  except
    on E: Exception do begin
      raise E;
      Exit;
    end;
  end;
end;

function TTBCAPageHandler.listFoods(ARawHtml: string): TStringList;
var
  code, food, group: string;
  doc         : THTMLDocument;
  tbody, tr   : TDOMNodeList;
  c, numRows  : integer;
  foodList    : TStringList;
begin
  try
    foodList:= TStringList.Create;
    ReadHTMLFile(doc,TStringStream.Create(ARawHtml));
    tbody:= doc.GetElementsByTagName('tbody');
    tr:= tbody[0].ChildNodes;
    numRows:= tr.Count;

    for c := 0 to numRows - 1 do
    begin
      code:= tr[c].GetChildNodes[0].TextContent;
      food:= tr[c].GetChildNodes[1].TextContent;
      group:= tr[c].GetChildNodes[3].TextContent;
      foodList.Add(code);
      foodList.Add(food);
      foodList.Add(group);
    end;
    result := foodList;
  except
    on E: Exception do
      raise E;
  end;
end;

function TTBCAPageHandler.listGrps(ARawHtml: string): TStringList;
var
  doc     : THTMLDocument;
  select  : TDOMNode;
  grpList : TStringList;
  c, rows : integer;
  grp, val: string;
begin
  ReadHTMLFile(doc,TStringStream.Create(ARawHtml));
  grpList := TStringList.Create;
  select  := doc.GetElementsByName('cmb_grupo')[0];
  rows    := select.ChildNodes.Count;

  for c:=1 to rows - 1 do begin // 0 = SELECIONE...
    grp   := select.ChildNodes[c].TextContent;
    val   := select.ChildNodes[c].Attributes[0].TextContent;
    grpList.Add(grp);
    grpList.Add(val);
  end;

  result:= grpList;
end;

function TTBCAPageHandler.listComps(ARawHtml: string): TStringList;
var
  comp, un, val : string;
  doc        : THTMLDocument;
  tbody, tr  : TDOMNodeList;
  c, numRows : integer;
  components : TStringList;
begin
  try
    components:= TStringList.Create;
    ReadHTMLFile(doc,TStringStream.Create(ARawHtml));
    tbody:= doc.GetElementsByTagName('tbody');
    tr:= tbody[0].ChildNodes;
    numRows:= tr.Count;

    for c := 1 to numRows - 1 do  // 0 = kJ
    begin
      comp := tr[c].GetChildNodes[0].TextContent;
      if not hasIN75Name(comp) then continue;

      un   := tr[c].GetChildNodes[1].TextContent;
      val  := tr[c].GetChildNodes[2].TextContent;
      val  := StringReplace(val, ',', '.', []);
      if (val = 'NA') or (val = 'tr') or (val = '-') then val := '0';
      components.Add(comp);
      components.Add(un);
      components.Add(val);
    end;

    result := components;
  except
    on E: Exception do begin
      raise E;
      Exit;
    end;
  end;
end;

function TTBCAPageHandler.hasIN75Name(var ATBCAComponent: string): Boolean;
var
  in75Names: array of string = (
    'Valor energético', 'Carboidratos', 'Açúcares adicionados', 'Proteínas',
    'Gorduras totais', 'Gorduras saturadas', 'Gorduras trans', 'Gorduras monoinsaturadas',
    'Gorduras poli-insaturadas', 'Ômega 6', 'Ômega 3', 'Colesterol', 'Fibras alimentares',
    'Sódio', 'Vitamina A', 'Vitamina D', 'Vitamina E', 'Vitamina K', 'Vitamina C', 'Tiamina',
    'Riboflavina', 'Niacina', 'Vitamina B6', 'Biotina', 'Ácido fólico', 'Ácido pantotênico',
    'Vitamina B12', 'Cálcio', 'Cloreto', 'Cobre', 'Cromo', 'Ferro', 'Flúor', 'Fósforo', 'Iodo',
    'Magnésio', 'Manganês', 'Molibdênio', 'Potássio', 'Selênio', 'Zinco', 'Colina'
  );
  tbcaNames: array of string = (
    'Energia', 'Carboidrato total', 'Açúcar de adição', 'Proteína',
    'Lipídios', 'Ácidos graxos saturados', 'Ácidos graxos trans', 'Ácidos graxos monoinsaturados',
    'Ácidos graxos poliinsaturados', '', '', 'Colesterol', 'Fibra alimentar',
    'Sódio', 'Vitamina A (RE)', 'Vitamina D', 'Alfa-tocoferol (Vitamina E)', '', 'Vitamina C', 'Tiamina',
    'Riboflavina', 'Niacina', 'Vitamina B6', '', 'Equivalente de folato', '',
    'Vitamina B12', 'Cálcio', '', 'Cobre', '', 'Ferro', '', 'Fósforo', '',
    'Magnésio', 'Manganês', '', 'Potássio', 'Selênio', 'Zinco', ''
  );
  idx: integer;
begin
  for idx:=0 to High(tbcaNames) do begin
    if ATBCAComponent = tbcaNames[idx] then begin
      ATBCAComponent:= in75Names[idx];
      Exit(true);
    end;
  end;
  Exit(false);
end;

constructor TTBCAPageHandler.Create;
begin
  FMainURL:= 'https://www.tbca.net.br/base-dados/';
  foodAndGroupURL:= FMainURL + 'composicao_estatistica.php';
  componentURL:= FMainURL + 'int_composicao_estatistica.php?cod_produto=';
  inherited Create;
end;

end.

